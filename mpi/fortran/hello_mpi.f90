PROGRAM hello_mpi
USE mpi

integer world_size, world_rank, name_len, ierror
character*(MPI_MAX_PROCESSOR_NAME) processor_name

! Initialize the MPI environment
call MPI_INIT(ierror)

! Get the number of processors 
call MPI_COMM_SIZE(MPI_COMM_WORLD, world_size, ierror)

! Get the process rank
call MPI_COMM_RANK(MPI_COMM_WORLD, world_rank, ierror)

! Get the processor name
call MPI_Get_processor_name(processor_name, name_len, ierror)

write(*,'(A,A,A,I2,A,I2,A)') 'Hello world from ', TRIM(processor_name),' rank ', world_rank,' out of ', world_size,' processors'

call MPI_FINALIZE(ierror)

END PROGRAM hello_mpi
