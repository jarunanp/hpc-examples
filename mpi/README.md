## Load module

```
$ env2lmod
$ module load gcc/6.3.0 openmpi/4.0.2
```

## To compile the C code

```
$ mpicc -o hello_mpi hello_mpi.c
```

## To compile the Fortran code

```
$ mpif90 -o hello_mpi hello_mpi.f90
```

## To run

```
$ bsub -n 4 -Is bash
$ mpirun -np 4 hello_mpi
Hello world from processor eu-ms-022-14, rank 0 out of 4 processors
Hello world from processor eu-ms-022-14, rank 1 out of 4 processors
Hello world from processor eu-ms-022-14, rank 2 out of 4 processors 
Hello world from processor eu-ms-022-14, rank 3 out of 4 processors
```

