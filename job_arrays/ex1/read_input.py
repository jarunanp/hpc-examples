import os

def myfunc(x):
    print("Input arguments: {}, {}\n".format(x[0], x[1]))
    return

with open("input.txt", "r") as f:
    data = f.readlines()

data_split=[]
for line in data:
    values = line.strip().split()
    data_split.append(values)
   
jobindex = int(os.environ["LSB_JOBINDEX"])
input_args = data_split[jobindex-1]
myfunc(input_args)
