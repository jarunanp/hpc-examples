// OpenMP header
#include <omp.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    #pragma omp parallel                   
    {
      printf("Hello world from thread %d\n", 
             omp_get_thread_num());
    }  
}
