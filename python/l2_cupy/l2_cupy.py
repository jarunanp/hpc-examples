import numpy as np 
import cupy as cp 
import sys

points = np.random.rand(10_000)

# Move data to GPU
x_gpu = cp.array(points)    
 
# Compute on the GPU
l2_gpu = cp.linalg.norm(x_gpu)  
   
# Return the array to host memory     
result = cp.asnumpy(l2_gpu)

print(result)
