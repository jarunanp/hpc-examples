import sys
import time
import numpy as np
from concurrent.futures import ProcessPoolExecutor

def accumulate_sum(v):
    sumv = 0
    for i in v:
        sumv += i
    return sumv

def main():

    n = 50_000_000
    vec = np.random.randint(0,1000,n)
    num_processes = int(sys.argv[1])
    n_per_process = int(n/num_processes) 
    vec_per_process = [vec[i*n_per_process:(i+1)*n_per_process] for i in range(num_processes)]
    
    # start the stop watch
    start = time.time()

    with ProcessPoolExecutor(max_workers=num_processes) as executor:
        results=executor.map(accumulate_sum, vec_per_process)

    # end the stop watch
    end = time.time()

    print("The accumulated sum is {:3.2e}".format(sum(results)))
    print("Elasped time: {:3.2f}".format(end-start))

if __name__ == '__main__':
    main()
